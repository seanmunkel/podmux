# Podmux

Podmux is a (very WIP) website designed to make it easier to manage complex podcast feeds.

It aims to take a single feed, and output multiple other feeds that can easily be subscribed to through an OPML file. This is especially helpful for patreon subscriptions that contain multiple shows in a single feed.

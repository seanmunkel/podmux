import { reactive } from "vue";
import { v4 as uuidv4 } from "uuid";
import { getCookieValue } from "../../util";

interface Filter {
  id: string;
  filter: string;
  name: string;
  type: string;
  output_feed: OutputFeed;
  case_sensitive: boolean;
  episodes: Episode[];
}

interface OutputFeed {
  id: string;
  name: string;
}

interface Episode {}

interface Podcast {
  name: string;
  url: string;
  private: boolean;
}

export class Editor {
  id: string | null;
  name: string = "";
  inputFeeds: Podcast[] = [];
  filters: Filter[] = [];
  changed: boolean = false;

  constructor(id: string | null = null) {
    this.id = id;
  }

  addFilter() {
    const filter = reactive({
      // TODO: Don't define IDs on frontend
      id: uuidv4(),
      filter: "",
      name: "",
      type: "starts-with",
      output_feed: {
        id: uuidv4(),
        name: "",
      },
      case_sensitive: false,
      episodes: [],
    });
    this.filters.push(filter);
    this.changed = true;
  }

  deleteFilter(id: string) {
    this.filters = this.filters.filter((it) => it.id !== id);
    this.changed = true;
  }

  addInputFeed() {
    this.inputFeeds.push({
      url: "",
      private: false,
      name: "",
    });
    this.changed = true;
  }

  deleteInputFeed(url: string) {
    this.inputFeeds = this.inputFeeds.filter((it) => it.url !== url);
    this.changed = true;
  }

  // API calls
  async save() {
    this.changed = true;
  }

  async fetch() {
    const resp = await fetch(`/api/mux/${this.id}`);
    if (!resp.ok) {
      throw Error("Fetching issues");
    }
    const body = await resp.json();
    this.name = body.name;
    this.inputFeeds = body.input_feeds;
    this.filters = body.filters;

    // This field isn't on the server side, so we must add it
    for (let f of this.filters) {
      f.episodes = [];
    }

    await this.updatePreview();
  }

  async updatePreview() {
    const payload = {
      name: this.name,
      input_feeds: this.inputFeeds,
      filters: this.filters,
    };

    const resp = await fetch("/api/preview/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": getCookieValue("csrftoken"),
      },
      body: JSON.stringify(payload),
    });
    if (resp.ok) {
      const body = await resp.json();
      for (let filter of this.filters) {
        filter.episodes = body[filter.output_feed.id];
      }
    }
  }
}

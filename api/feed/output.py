from xml.etree import ElementTree as ET

from feedparser import FeedParserDict

from api.models import OutputFeed, PodMux

ITEM_TAGS = {
    "title": "title",
    "link": "link",
    "summary": "summary",
}


def render_feed(feed: OutputFeed, entries: list[FeedParserDict]) -> str:
    root = ET.Element("rss")
    channel = ET.SubElement(root, "channel")
    ET.SubElement(channel, "title").text = feed.name

    for entry in entries:
        item = ET.SubElement(channel, "item")
        for tag, attr in ITEM_TAGS.items():
            ET.SubElement(item, tag).text = getattr(entry, attr, None)

    return ET.tostring(root, encoding="utf-8", method="xml")


def render_opml(mux: PodMux, base_url: str) -> str:
    root = ET.Element("opml")
    root.set("version", "1.0")

    head = ET.SubElement(root, "head")
    title = ET.SubElement(head, "title")
    title.text = mux.name

    body = ET.SubElement(root, "body")
    outline = ET.SubElement(body, "outline")
    outline.set("text", "feeds")

    for filtered in mux.filters.all():
        title = filtered.output_feed.name
        url = f"{base_url}api/feed/{filtered.output_feed.id}"
        feed_outline = ET.SubElement(outline, "outline")
        feed_outline.set("type", "rss")
        feed_outline.set("text", title)
        feed_outline.set("xmlUrl", url)

    return ET.tostring(root, encoding="utf-8", method="xml")

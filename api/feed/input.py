import dramatiq
from xml.etree import ElementTree
import requests

from api.models import InputFeed

ITUNES_BLOCK = "{http://www.itunes.com/dtds/podcast-1.0.dtd}block"
PLAY_BLOCK = "'{http://www.google.com/schemas/play-podcasts/1.0}block'"


# Per the warning at the top of ElementTree, this can be used for a
# DOS attack, so that needs to be addressed with a reasonable timeout
@dramatiq.actor
def check_private(input_feed: InputFeed):
    resp = requests.get(input_feed.url)
    resp.raise_for_status()
    tree = ElementTree.fromstring(resp.content)
    channel = tree.find("channel")
    itunes_block = channel.find(ITUNES_BLOCK)
    play_block = channel.find(PLAY_BLOCK)

    blocks = set()
    if itunes_block is not None:
        blocks.add(itunes_block.text.lower())
    if play_block is not None:
        blocks.add(play_block.text.lower())

    # If anything is marked to block, this will be private
    input_feed.private = "yes" in blocks
    input_feed.save()


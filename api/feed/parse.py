from collections import defaultdict
from dataclasses import dataclass
from typing import Iterable

import feedparser
import requests
from feedparser.util import FeedParserDict

from api.models import FilterType, PodcastFilter, PodMux, PodMuxSubscription


@dataclass
class RenderedPodMux:
    input_feeds: list[str]
    filters: list[PodcastFilter]


def _download_feeds(input_feeds: Iterable[str]) -> list[FeedParserDict]:
    feeds = []

    for input_feed in input_feeds:
        resp = requests.get(input_feed)
        resp.raise_for_status()
        feed = feedparser.parse(resp.content)
        feeds.append(feed)

    return feeds


def filter_entries(
    mux: PodMux | PodMuxSubscription | RenderedPodMux,
) -> dict[str, list]:
    # Convert anything we get into a RenderedPodMux
    if isinstance(mux, PodMux):
        mux = RenderedPodMux(
            input_feeds=[f.url for f in mux.input_feeds.all()],
            filters=list(mux.filters.all()),
        )
    elif isinstance(mux, PodMuxSubscription):
        input_feeds = [
            *[f.url for f in mux.original.input_feeds.filter(private=False)],
            *[f.url for f in mux.private_feeds.all()],
        ]
        mux = RenderedPodMux(
            input_feeds=input_feeds,
            filters=(mux.original.filters.all()),
        )

    input_feeds = _download_feeds(mux.input_feeds)
    output_feeds = defaultdict(list)

    filters = sorted(mux.filters, key=lambda it: it == FilterType.FALLBACK)

    for feed in input_feeds:
        for entry in feed.entries:
            for input_filter in filters:
                if input_filter.matches(entry):
                    key = str(input_filter.output_feed.id)
                    output_feeds[key].append(entry)
                    break

    return dict(output_feeds)

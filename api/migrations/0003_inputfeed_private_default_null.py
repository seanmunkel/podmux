# Generated by Django 4.0.3 on 2022-05-08 18:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_subscription'),
    ]

    operations = [
        migrations.AlterField(
            model_name='inputfeed',
            name='private',
            field=models.BooleanField(default=None, null=True),
        ),
    ]

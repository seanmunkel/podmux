from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from rest_framework.routers import DefaultRouter

from api.views import PodMuxSubscriptionViewSet, PodMuxViewSet, preview, rss_feed

router = DefaultRouter()
router.register("mux", PodMuxViewSet, basename="mux")
router.register("sub", PodMuxSubscriptionViewSet, basename="sub")

urlpatterns = [
    path("feed/<feed_id>", rss_feed),
    path("preview/", csrf_exempt(preview)),
    *router.urls,
]

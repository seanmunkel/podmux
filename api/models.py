import uuid

import feedparser.util
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _


class FilterType(models.TextChoices):
    """Filter types (based on episode titles)"""

    STARTS_WITH = "sw", _("Starts with")
    CONTAINS = "in", _("Contains")
    ENDS_WITH = "ew", _("Ends with")
    FALLBACK = "fb", _("Fallback")


class PodMux(models.Model):
    """
    A set of input feeds, their filters and the output feeds.

    This is the core unit in our application.
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=32)
    user = models.ForeignKey(to=User, on_delete=models.SET_NULL, null=True)
    public = models.BooleanField(default=False)


class OutputFeed(models.Model):
    """An RSS feed outputted by podmux, per user specifications"""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=32)
    art = models.URLField(null=True, blank=True)
    podmux = models.ForeignKey(
        to=PodMux, on_delete=models.CASCADE, related_name="output_feeds"
    )

    def __str__(self):
        return self.name


class InputFeed(models.Model):
    """An RSS feed provided by the user"""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=32)
    url = models.URLField()
    # First we set this to None to designate that it has not been verified
    # to be a public or private feed. A user should never be able to override
    # the private nature of the feed.
    private = models.BooleanField(null=True, default=None)
    podmux = models.ForeignKey(
        to=PodMux, on_delete=models.CASCADE, related_name="input_feeds"
    )


class PodcastFilter(models.Model):
    """A filter for an InputFeed provided by the user"""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    filter = models.CharField(max_length=128)
    type = models.CharField(
        max_length=2,
        choices=FilterType.choices,
    )
    output_feed = models.ForeignKey(to=OutputFeed, on_delete=models.CASCADE)
    case_sensitive = models.BooleanField(default=False)
    podmux = models.ForeignKey(
        to=PodMux,
        on_delete=models.CASCADE,
        related_name="filters",
    )

    def matches(self, entry: feedparser.util.FeedParserDict):
        title: str = entry.title
        title_filter = self.filter
        if not self.case_sensitive:
            title = title.casefold()
            title_filter = title_filter.casefold()

        match self.type:
            case FilterType.STARTS_WITH:
                return title.startswith(title_filter)
            case FilterType.ENDS_WITH:
                return title.endswith(title_filter)
            case FilterType.CONTAINS:
                return title_filter in title
            case FilterType.FALLBACK:
                return True


class PodMuxSubscription(models.Model):
    """A user's subscription to a 'template' podmux"""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    original = models.ForeignKey(
        to=PodMux, on_delete=models.CASCADE, related_name="subscriptions"
    )
    user = models.ForeignKey(to=User, on_delete=models.SET_NULL, null=True)

    def _missing_feed_ids(self) -> set[uuid.UUID]:
        expected_ids = {
            i.id for i in self.original.input_feeds.only("id", "private") if i.private
        }
        private_ids = {
            i.input_feed_id
            for i in PrivateFeed.objects.filter(subscription=self).only("input_feed_id")
        }
        return expected_ids - private_ids

    @property
    def is_complete(self) -> bool:
        return len(self._missing_feed_ids()) == 0

    @property
    def missing_feeds(self):
        ids = self._missing_feed_ids()
        return InputFeed.objects.filter(id__in=ids)


class PrivateFeed(models.Model):
    """
    A user-specific version of a feed.

    For patreon and other private subscriptions it is necessary to make it
    hard/impossible to accidentally share these links and upset to creators.
    (We don't want this site to be easily used for piracy)
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    subscription = models.ForeignKey(
        to=PodMuxSubscription, on_delete=models.CASCADE, related_name="private_feeds"
    )
    input_feed = models.ForeignKey(
        to=InputFeed, on_delete=models.CASCADE, limit_choices_to={"private": True}
    )
    url = models.URLField()

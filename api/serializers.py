from copy import copy

from rest_framework import serializers

from api.feed.parse import RenderedPodMux
from api.models import InputFeed, OutputFeed, PodcastFilter, PodMux, PodMuxSubscription


class OutputFeedSerializer(serializers.ModelSerializer):
    class Meta:
        model = OutputFeed
        fields = ("id", "name", "art")


class InputFeedSerializer(serializers.ModelSerializer):
    class Meta:
        model = InputFeed
        fields = ("id", "name", "url", "private")
        read_only_fields = ("private",)


class PodcastFilterSerializer(serializers.ModelSerializer):
    output_feed = OutputFeedSerializer()

    class Meta:
        model = PodcastFilter
        fields = ("id", "filter", "type", "case_sensitive", "output_feed")


class PodMuxSerializer(serializers.ModelSerializer):
    input_feeds = InputFeedSerializer(many=True)
    filters = PodcastFilterSerializer(many=True)

    class Meta:
        model = PodMux
        fields = ("id", "name", "input_feeds", "filters")

    def create_temp(self, validated_data) -> RenderedPodMux:
        input_feeds = [d["url"] for d in validated_data["input_feeds"]]
        filters = []
        for data in validated_data["filters"]:
            data = copy(data)
            of_data = data.pop("output_feed")
            # TODO: Remove this on the front end, instead of here
            data.pop("episodes")

            output_feed = OutputFeed(**of_data)
            filters.append(PodcastFilter(**data, output_feed=output_feed))

        return RenderedPodMux(input_feeds, filters)


class SummaryPodMuxSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source="user.username")

    class Meta:
        model = PodMux
        fields = ("id", "name", "user")


class PodMuxSubscriptionSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source="original.name")

    class Meta:
        model = PodMuxSubscription
        fields = ("id", "name")

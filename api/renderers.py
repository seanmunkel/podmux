from rest_framework import renderers


class XmlRenderer(renderers.BaseRenderer):
    """Used to output opml feeds"""

    media_type = "application/xml"
    format = ""

    def render(self, data, accepted_media_type=None, renderer_context=None):
        return data

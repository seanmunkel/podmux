from django.conf import settings
from django.core.exceptions import PermissionDenied
from rest_framework import viewsets
from rest_framework.decorators import action, api_view, renderer_classes
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from api.feed.output import render_feed, render_opml
from api.feed.parse import filter_entries
from api.models import OutputFeed, PodMux, PodMuxSubscription
from api.renderers import XmlRenderer
from api.serializers import (
    PodMuxSerializer,
    PodMuxSubscriptionSerializer,
    SummaryPodMuxSerializer,
)


class PodMuxViewSet(viewsets.ModelViewSet):
    def get_serializer_class(self):
        if self.action == "retrieve":
            return PodMuxSerializer
        else:
            return SummaryPodMuxSerializer

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return PodMux.objects.filter(user=self.request.user)
        else:
            return PodMux.objects.none()

    def get_object(self):
        obj = super().get_object()

        if obj.user != self.request.user:
            raise PermissionDenied()

        return obj

    @action(detail=True, methods=["GET"], renderer_classes=(XmlRenderer,))
    def opml(self, request, pk=None):
        mux = self.get_object()
        body = render_opml(mux, settings.BASE_URL)
        return Response(body)


class PodMuxSubscriptionViewSet(viewsets.ModelViewSet):
    serializer_class = PodMuxSubscriptionSerializer

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return PodMuxSubscription.objects.filter(user=self.request.user)
        else:
            return PodMuxSubscription.objects.none()


@api_view(["GET"])
@renderer_classes([XmlRenderer])
def rss_feed(request, feed_id: str):
    feed = get_object_or_404(OutputFeed, pk=feed_id)
    all_entries = filter_entries(feed.podmux)
    body = render_feed(feed, all_entries[feed.id])
    return Response(body)


@api_view(["POST"])
def preview(request):
    serializer = PodMuxSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    mux = serializer.create_temp(request.data)
    return Response(filter_entries(mux))

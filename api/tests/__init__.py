import pathlib
from unittest import mock

import feedparser
from django.test import TestCase
from feedparser import FeedParserDict

from api.feed.parse import filter_entries
from api.models import (
    FilterType,
    InputFeed,
    OutputFeed,
    PodcastFilter,
    PodMux,
    PodMuxSubscription,
    PrivateFeed,
)


class TestPodcastFilter(TestCase):
    def setUp(self):
        self.of = OutputFeed(name="PlaceHolder")

    def test_startswith_insensitive(self):
        filter1 = PodcastFilter(
            type=FilterType.STARTS_WITH,
            filter="Pancake",
            case_sensitive=False,
            output_feed=self.of,
        )
        good_titles = [
            "Pancakes and Waffles",
            "Pancake",
            "Pancakeeeee" "pancake",
            "pancakeeeee",
            "pancakes are sad",
        ]
        for title in good_titles:
            entry = FeedParserDict(title=title)
            self.assertTrue(filter1.matches(entry))

        bad_titles = [" Pancake", "Good are Pancakes", "", "Alabama"]
        for title in bad_titles:
            entry = FeedParserDict(title=title)
            self.assertFalse(filter1.matches(entry))

    def test_startswith_sensitive(self):
        filter1 = PodcastFilter(
            type=FilterType.STARTS_WITH,
            filter="Pancake",
            case_sensitive=True,
            output_feed=self.of,
        )
        good_titles = ["Pancakes and Waffles", "Pancake", "Pancakeeeee"]
        for title in good_titles:
            entry = FeedParserDict(title=title)
            self.assertTrue(filter1.matches(entry))

        bad_titles = [
            " Pancake",
            "Good are Pancakes",
            "",
            "Alabama",
            "pancake",
            "pancakeeeee",
            "pancakes are sad",
        ]
        for title in bad_titles:
            entry = FeedParserDict(title=title)
            self.assertFalse(filter1.matches(entry))


class ParseTest(TestCase):
    fixtures = ["parse"]

    def setUp(self):
        test_dir = pathlib.Path(__file__).parent / "test_data"

        with open(test_dir / "androids-and-aliens.xml") as gcp_file:
            self.ana_xml = gcp_file.read()
        with open(test_dir / "cannonfodder.xml") as gcp_file:
            self.fod_xml = gcp_file.read()

    @mock.patch("api.feed.parse._download_feeds")
    def test_filter_entries(self, mock_download: mock.MagicMock):
        mock_download.return_value = [
            feedparser.parse(self.ana_xml),
            feedparser.parse(self.fod_xml),
        ]

        gcp: PodMux = PodMux.objects.first()
        entries = filter_entries(gcp)

        self.assertEqual(4, len(entries.keys()))

        pairs = [
            ("Androids and Aliens", 155),
            ("Inherit the Sand", 7),
            ("Haunted City", 4),
            ("Cannon Fodder", 93),
        ]

        for feed_name, count in pairs:
            feed = OutputFeed.objects.get(name=feed_name)
            self.assertEqual(count, len(entries[str(feed.id)]))


class PrivateFeedTest(TestCase):
    fixtures = ["parse"]

    def setUp(self):
        self.original: PodMux = PodMux.objects.first()
        self.sub = PodMuxSubscription.objects.create(original=self.original)

    def test_is_complete_empty(self):
        self.assertTrue(self.sub.is_complete)

    def test_is_complete_single(self):
        feed: InputFeed = self.original.input_feeds.first()
        feed.private = True
        feed.save()

        self.assertFalse(self.sub.is_complete)

        PrivateFeed.objects.create(
            subscription=self.sub,
            input_feed=feed,
            url="https://patreon.com/private-feed",
        )

        self.assertTrue(self.sub.is_complete)

    def test_missing_feeds_empty(self):
        self.assertFalse(self.sub.missing_feeds.exists())

    def test_missing_feeds_single(self):
        feed: InputFeed = self.original.input_feeds.first()
        feed.private = True
        feed.save()

        missing = self.sub.missing_feeds
        self.assertTrue(missing.exists())

        self.assertEqual(list(missing), [feed])

        PrivateFeed.objects.create(
            subscription=self.sub,
            input_feed=feed,
            url="https://patreon.com/private-feed",
        )

        self.assertFalse(self.sub.missing_feeds.exists())

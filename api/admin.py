from django.contrib import admin

from api import models


class OutputFeedInline(admin.StackedInline):
    model = models.OutputFeed


class InputFeedInline(admin.StackedInline):
    model = models.InputFeed


class PodcastFilterInline(admin.StackedInline):
    model = models.PodcastFilter


@admin.register(models.PodMux)
class PodMuxAdmin(admin.ModelAdmin):
    inlines = [OutputFeedInline, InputFeedInline, PodcastFilterInline]
    list_display = ["name", "user"]


@admin.register(models.PodMuxSubscription)
class PodMuxSubscriptionAdmin(admin.ModelAdmin):
    pass
